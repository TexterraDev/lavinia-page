$(document).ready(function() {

	// Вызываем мобильное меню
	$('.hamburger').click(function(){
		$(this).toggleClass('is-active');
		var thisParent = $(this).parent('.mobile__menu__trigger');
		thisParent.siblings('.main__menu').toggleClass('opened');
	});

	// Лепим по скроллу меню к верхней границе экрана
	var headerAll = $('.main__menu__wrap');
	var headerHeight = headerAll.outerHeight();
	$(window).scroll(function(){
		
		var winOffsetTop = $(this).scrollTop();
		if (winOffsetTop > headerHeight) {
			headerAll.addClass('fixed');
			// Задаём хэдеру минимальную высоту, чтобы предотвратить скачок документа
			$('header').css('min-height', headerHeight);
		} else {
			headerAll.removeClass('fixed');
			$('header').removeAttr('style');
		}
	});

	// Анимация появления поиска на десктопе
	var winWidth = $(window).width();
	if (winWidth > 1199) {
		var searchButton = $('.main__menu__search__submit');
		var searchInput = searchButton.siblings('input[type="search"]');

		$('.main__menu__search__submit').click(function(evt) {

			var searchInputVal = searchInput.val();

			if ($(this).hasClass('search') && searchInputVal == "") {
				evt.preventDefault();
				$(this).removeClass('search');
				$(this).parents('.main__menu, .footer__nav__wrap').removeClass('searched');
			} else if ($(this).hasClass('search') && searchInputVal != "") {

			} else {
				evt.preventDefault();
				$(this).addClass('search');
				$(this).parents('.main__menu, .footer__nav__wrap').addClass('searched');
			}
		})
	}

	// Swiper slider reviews
	var swiper = new Swiper('.swiper-container', {
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
	$(".main__menu__nav__item").click(function(){
		$('html, body').animate({scrollTop: $($.attr(this, 'href')).offset().top - 50}, 750);
		$('.main__menu.opened').removeClass('opened');
		$('.hamburger.is-active').removeClass('is-active');
	});
		

  function showThanks (element) {
  	element.addClass('animated');
  	element.siblings('.lp__form, .middle__form__title, .reviews__form__title').hide();
  	element.parent('.middle__form__container').addClass('thanks');
  }

});